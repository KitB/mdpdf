#!/usr/bin/env python
import markdown2
from xhtml2pdf import pisa


def md2pdf(md, f):
    html = markdown2.markdown(md)
    pisa.CreatePDF(html, f)


def main():
    import sys
    md = file(sys.argv[1], 'r').read()
    output = file(sys.argv[2], 'wb')
    md2pdf(md, output)

if __name__ == "__main__":
    main()
