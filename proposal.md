% Programming as an Interactive Experience
% Kit Barnes
% January 2013

# Introduction
In January 2012 Bret Victor gave a talk entitled "Inventing on Principle"
[@Inventing] in which he demonstrated a series of interactive coding suites.
These suites were designed to address the problem of programmers in creative,
graphical contexts (such as animation, styling, and game design) having to run
their code after writing it in order to see the changes they have made. His
applications give immediate feedback to the programmer without requiring the
programmer to create a harness specifically for this purpose.

In creative contexts decisions are very often made as aesthetic judgements by
comparing versions of the code against each other; an example of this would be
adjusting the parameters of gravity and movement speed or even changing the
core movement behaviour in a platforming game (such as not allowing the player
to move left) [@Inventing]. A programmer would have to produce multiple
versions of the code and run them against each other for comparison. Immediate
feedback would allow the programmer to quickly search the parameter space.

A more explored example is that of performance coding in which programmers
write a program on stage to produce usually a combination of visual and musical
results. Such performances are often referred to as Live Coding and, though not
the goal of this project, will quite possibly be a viable use for it.

In the context of robotics an immediate feedback system could be used when
tuning the parameters of a PID (Proportional, Integral, Derivative) process.
PID is a mechanism by which one can compensate for an oscillating error in a
control process such as the angle between the bearing of a robot and its
desired bearing. PID requires that the programmer select suitable coefficients
for the error function, its integral, and its derivative (all calculated
numerically) to produce a converging oscillation as the output function.
Selection of these parameters is typically done by hand and is involved in a
long-running process.

Similarly in computer vision a common technique for improving effectiveness in
constrained situations is to use fixed threshold parameters for choosing
coloured regions of the image feed; these parameters are often tuned by hand
and could be improved with immediate feedback.

Viewing immediate feedback as a system for maintaining state we can see that it
will also allow a programmer working on a long-running, stateful application to
investigate changes in that application without requiring them to restart the
application and rebuild the state [@DSU] (particularly useful if the state requires
that the process has been running for a long time).

# Background
There are many tools and systems that perform similar tasks to or subtasks of
this project. This section provides an overview of projects that may inform the
design of the system for this project. They may also constitute a set of
projects against which this project can be evaluated.

There have been many attempts at similar systems from many disciplines.
Following are brief descriptions of a fairly representative sample of these
systems.

Bret Victor [@Victor] demonstrated a number of systems dedicated to specific
tasks; he did not release any source code for these systems and I believe they
were orchestrated examples.

On the other hand there have been attempts to recreate specific examples from
Victor's talk. `livecoding.io` [@Livecoding] provides a web-page view along
with an editor allowing the programmer to modify the HTML, Javascript, CSS, and
JSON source code for the document. As the code is modified the page is
re-rendered to reflect this.

Daniel Hooper's Codebook [@Codebook] provides a similar interface though it only allows the
use of Javascript to draw onto a canvas element. It also implements the
graphical inspection feature demonstrated by Victor, allowing the programmer to
select an element of the drawn image using the mouse and have the program
highlight the line of code responsible for drawing that element.

There exists a sizeable community of performance programming enthusiasts who
produce music and videos on stage using specialised programming environments.
These projects almost invariably have a simple looping execution structure and
avoid stateful computation.

E15:oGFx[@E15] is one such project that allows programmers to draw on a 2D
canvas to produce 3D shapes. scripts for it are written in Python and need only
provide a draw function. The software then uses this draw function to draw a 2D
scene each frame. To perform the update of drawing, this software simply
re-runs the script when the user requests that it do so.

Similarly, Fluxus[@Fluxus] is a live coding environment for audio-sensitive 3D
applications. It allows the programmer to register a function to be run each
frame and produces a scene from this function. In lieu of performing updates to
the running script, the script is simply recompiled and run from the beginning
when the user presses F5.

The livecoding module for Python [@Livepython] implements a custom module
mechanism that allows it to watch files for changes and update according to
those changes. This module supposedly performs the limited state transformation
required to simply begin running a new version of the code but is unlikely to
successfully adapt between data structures. This module has not been updated
since 2010 and does not appear to run in modern Python distributions so this
analysis is based upon code inspection rather than real testing.

On a production server it is desirable that a piece of software be always
available; these systems allow long-running software to be updated without
requiring the software to stop running in the process. For software running on
a single machine (that is to say with no fallback machine) this is vital if a
system administrator wishes to approach 100% uptime [@DSU].

<!-- This bit not quite done
     Ask Ian what needs doing-->

The dynamic software update tradition centred on Hicks' thesis[@DSU] provides a
clear theoretical framework for discussing stateful software update in as safe
a manner as possible. The original paper also contains a section on how to
perform state transformation inference. The system provided by Hicks requires
that the programmer use a new C-like language designed for this purpose [@DSU].

The Erlang programming language provides utilities that allow software to be
updated in much the same way as Hicks. These utilities require that the
programmer write in a particular style and using particular libraries; they
also require the programmer to provide state transition functions[@Erlang].


# Identifying the Project
When this project began, two potential foci for evaluation were identified:

* Using immediate feedback to in some way improve the rate at which people
  learn to program. This would require that the system focus on producing
  simple programs with well defined "correct" ways of doing each thing that
  could be done. This would allow the system to be much more constrained in
  what it can produce but would require that special effort be put into
  ensuring its simplicity and robustness against errors.
* Using immediate feedback to in some way improve the experience of programming
  for those who are already adept. This would require the system to address
  more sophisticated concerns and allow the programmer more freedom -- both in
  what they can produce and what errors they can cause.

The latter focus has been selected for this project.

The task of providing generic immediate feedback was considered to be too great
for the allotted time so it was decided that a library for producing 2D games
be produced, allowing the domain of updates to be limited somewhat. Games
were selected as they provide an obvious representation and update mechanism --
we run the game and ensure that future iterations of the main loop respect the
changes that have been made. These games will be limited to two dimensions
because a 3D game library would require exponentially more work.

With these considerations, the following aim for the project was settled upon:


> **Provide a system by which a 2D game can be written and rewritten whilst it
  is running and have the running game be updated as the source code is
  modified.**


In accordance with this an API was designed along with a description of a
minimal usable interface that would constitute completion of the task.

Python has been selected as a programming language for this project due to its
inherently dynamic nature and popularity; meaning it will be easier to produce
an interactive programming system that will be useful to many people. Though it
was decided that the outcome of the project be an API; Python also provides the
PyPy runtime environment, which allows extensions to the Python language to be
written in a subset of Python called RPython. This gives access to the abstract
syntax tree which may be useful for providing selective recompilation.

With Python's dynamic capabilities an API was selected as the easier path to
fulfilling the aim of the project.

To produce the game library aspect it was
necessary to choose a library for graphical output in Python. There are two
large projects dedicated to producing game-like systems in Python:

* **Pygame**[@Pygame]: A mature project based upon the SDL project providing a strong
              history of use in 2D graphical applications. Pygame provides
              higher-level drawing primitives and has built-in sprite handling
              mechanisms.
* **Pyglet**[@Pyglet]: A younger project that provides a small amount of abstraction
              over raw OpenGL. Pyglet is provides much greater capabilities
              including faster programs and rendering using dedicated GPUs on
              all platforms. It is more difficult to understand and use
              however.

Pygame has been tentatively selected due to my greater familiarity with it
along with some libraries I have produced for it that are steps along the way
to the full game library this project will produce.

# Specification and evaluation criteria

Provided in Appendix A is a series of source files for systems that this
project is expected to be able to update in real time. The system is also
expected to be capable of transforming each of the single file examples into
each of the other examples.

There are single-file examples running through a series of transformations on a
single file to produce an interactive toy program in which a player controls a
bouncing ball. They start with ball-drawing and gradually add features -- such
as movement and gravity -- until the program is finished.

These single-file examples can also be considered an expected session of the
system, with a programmer starting with no code in the file then writing each
successive file from the last. The system is expected to be able to react
appropriately to this. Within each example the system will also be expected to
be able to deal with the programmer changing the values of any variables --
such as the radius and colour of the ball -- and the behaviour of functions --
such as changing the circle into a square in the `draw` function. Importantly
if the programmer changes the values of `x` and `y` -- values that are
initialized then used later -- then the system will move the ball to the new
position. This is as this would be useful behaviour for a games programmer
trying to test their game.

Should the programmer at any point cause an error in the code -- including
syntax and exceptions, but not bugs that do not cause exceptions -- the system
should begin executing using the previous working source code.

There is also a multiple-file example which constitutes a more complex version
of this toy program which will require the system to track more than one
changing module as well as handling transitions of states in classes rather
than simple variables. In particular the system will be capable of the same
arbitrary updates to methods that it will be capable of with top-level
functions.

Following are key stages of development that will constitute stages of
evaluation; split into two largely orthogonal dimensions:

## Levels of interactivity
These stages represent increasingly interactive systems, though they say
nothing about any limitations upon language features that the programmer may
use.

The system will be able to:

* Observe programs and run them within a harness without change.
* Make changes from one program to another with assistance from the programmer
  in defining state transformations.
* Make changes from one program to another with little assistance from the
  programmer.

## Levels of language usage
These stages represent increasingly complex programs that can be transformed by
the system.

The system will be able to:

* Transform top-level variables such as `x`, `y`, and `gravity` in the
  single-file examples.
* Transform function definitions such as `draw` in the single-file examples.
  This includes modifying variables within functions and nested functions.
* Introduce new top-level functions and variables.
* Transform code from multiple modules.
* Transform instances of classes into instances of new classes.

# Work Plan
This project will be incrementally completed in the following stages over the
full duration of the project:

* An API stub that will simply run programs without transforming them.
* An API which provides real time update. Starting with top-level variables and
  working through the levels of language usage up to transforming multiple
  modules.
* An interface that makes use of this API.
* A refinement of this library allowing for state transformation where
  transformation functions are provided by the programmer. This will include
  transforming classes using programmer provided transformation functions.
* A suite of prebuilt state transformation functions for common cases that can
  be automatically inferred. Importantly providing inference of class
  transformations.

This project will produce at its conclusion an API that will allow for live
coding environments using a library for 2D games to be produced. The API will
consist of the following functions:

* `watch`: Begin bookkeeping on a large unit of code (in Python this would be a
           module, in Java a class).
* `start`: Begin program execution. Possibly taking as a parameter a function
           at which to begin execution.
* `stop`: Cease program execution.
* `update`: Taking the new source code as input, modify the running program to
            reflect it.
* `pause`: Cease program execution in such a way that it can be resumed from
           its current state.

Using this, a minimal interface will be produced that allows a programmer to
write source code in any editor they desire; when the source files are saved to
disk a watching process will update the running program from them.

In the second year of the project the interface will be enhanced to include a
graphical text editor which updates the code when the programmer ceases typing.
This will mostly likely be in the form of a plugin for an existing editor,
rather than an entirely new editor.

# Time Plan

-------------------------------------------------------------------------------
Implementation                          Write Up
------------------------------------    ---------------------------------------
By the end of January a rudimentary     In January this report will be finished
system allowing transformations on      and brief descriptions of any attempted
single-file applications will be        implementation mechanisms will be
produced; this system will have no      produced to assist in writing later
guarantee of correctness but should be  reports.
considered as a valid proof of concept
for the selected implementation
mechanism. The purpose of this work
will be to explore mechanisms for
implementing the system and inform my
continued implementation planning.



By the end of February a system         In February an outline of the interrim
allowing transformations on             report will be produced, detailing
multiple-file applications will be      sections to be included and including
produced. Again this system should be   notes of important facts discovered
considered a proof of concept.          during the implementation.

During March the systems will be        In March the report will be written,
refined so that they work reliably;     starting with a draft in early March
possibly including the addition of      and refining that draft throughout the
state transformation mechanisms.        month such that the report should be
                                        viable for submission by the end of the
                                        second week of March.

By the end of semester three state      In semester three an outline of the
transformations will definitely be      final report will be produced to the
added. An interface for using the       same level of detail as the interrim
system will also be produced.           report outline.

In semester four state transition       Throughout semester four the final
inference will be added to the          report will be written.
project.



-------------------------------------------------------------------------------

\newpage

# Appendix A

## Single file examples

### Simple drawing

~~~~{.python}
# File: ball.py
# Draws a ball on the screen
import pygame


# Some colours
# These will be transformable
black = (0, 0, 0)
purple = (116, 4, 181)

pygame.init()
# This next line will be hard to transform
# though I would like to try
screen = pygame.display.set_mode((640, 480))

ball_pos = (120, 120)
radius = 40

# This will be transformable
def darken((R, G, B)):
    """ Make a colour a little darker """
    return (R / 4, G / 4, B / 4)


def draw():
    pygame.draw.circle(screen, darken(purple), ball_pos, radius)
    pygame.draw.circle(screen, purple, ball_pos, radius, 1)

done = False
while not done:
    # These lines would be nice to be able to transform, but difficult
    screen.fill(black)
    draw()
    pygame.display.flip()
~~~~

------------------------------------------------------------------------------

### Tracking basic state and introducing a function
This file involves top-level variables (rather than constants masquerading as
variables) with changing state that will need to be appreciated by the system.

It will also require that the system is capable not only of modifying existing
functions but introducing new functions.

~~~~{.python}
# File: motion.py
# Adds velocity and damping (via friction) to the ball
import pygame


# Some colours
black = (0, 0, 0)
purple = (116, 4, 181)

pygame.init()
screen = pygame.display.set_mode((640, 480))
clock = pygame.time.Clock()

ball_pos = (120, 120)
ball_velocity = (40, 50)
friction = 0.5
radius = 40


def darken((R, G, B)):
    """ Make a colour a little darker """
    return (R / 4, G / 4, B / 4)


def draw():
    pos = [int(round(n)) for n in ball_pos]
    pygame.draw.circle(screen, darken(purple), pos, radius)
    pygame.draw.circle(screen, purple, pos, radius, 1)


def update_ball():
    global ball_pos, ball_velocity
    dt = clock.tick(60)
    dt_seconds = dt / 1000.0
    ball_pos = [a + (b * dt_seconds) for (a, b) in zip(ball_pos, ball_velocity)]
    ball_velocity = [n * (1 - (friction * dt_seconds)) for n in ball_velocity]


done = False
while not done:
    update_ball()
    screen.fill(black)
    draw()
    pygame.display.flip()
~~~~

------------------------------------------------------------------------------

### Modifying a function

~~~~{.python}
# File gravity.py
# Adds a constant downward acceleration to the ball
import pygame


# Some colours
black = (0, 0, 0)
purple = (116, 4, 181)

pygame.init()
screen = pygame.display.set_mode((640, 480))
clock = pygame.time.Clock()

ball_pos = (120, 120)
ball_velocity = (0, 0)
gravity = 150
friction = 0.5
radius = 40


def darken((R, G, B)):
    """ Make a colour a little darker """
    return (R / 4, G / 4, B / 4)


def draw():
    pos = [int(round(n)) for n in ball_pos]
    pygame.draw.circle(screen, darken(purple), pos, radius)
    pygame.draw.circle(screen, purple, pos, radius, 1)


def update_ball():
    global ball_pos, ball_velocity
    dt = clock.tick(60)
    dt_seconds = dt / 1000.0
    ball_pos = [a + (b * dt_seconds) for (a, b) in zip(ball_pos, ball_velocity)]
    ball_velocity = [n * (1 - (friction * dt_seconds)) for n in ball_velocity]
    ball_velocity[1] += gravity * dt_seconds


done = False
while not done:
    update_ball()
    screen.fill(black)
    draw()
    pygame.display.flip()
~~~~

------------------------------------------------------------------------------


### Including branching code

~~~~{.python}
# File: bouncing.py
# Makes the ball bounce off of the edges of the window
import pygame


# Some colours
black = (0, 0, 0)
purple = (116, 4, 181)

pygame.init()
dims = (640, 480)
screen = pygame.display.set_mode(dims)
clock = pygame.time.Clock()

ball_pos = (120, 120)
ball_velocity = (800, -400)
gravity = 200
friction = 0.4
elasticity = 0.7
radius = 40


def darken((R, G, B)):
    """ Make a colour a little darker """
    return (R / 4, G / 4, B / 4)


def draw():
    pos = [int(round(n)) for n in ball_pos]
    pygame.draw.circle(screen, darken(purple), pos, radius)
    pygame.draw.circle(screen, purple, pos, radius, 5)


def update_ball():
    global ball_pos, ball_velocity
    dt = clock.tick(60)
    dt_seconds = dt / 1000.0
    ball_pos = [a + (b * dt_seconds) for (a, b) in zip(ball_pos, ball_velocity)]
    ball_velocity = [n * (1 - (friction * dt_seconds)) for n in ball_velocity]
    ball_velocity[1] += gravity * dt_seconds
    if ball_pos[1] < radius and ball_velocity[1] < 0:
        ball_velocity[1] *= -elasticity
    elif ball_pos[1] > (dims[1] - radius) and ball_velocity[1] > 0:
        ball_velocity[1] *= -elasticity

    if ball_pos[0] < radius and ball_velocity[0] < 0:
        ball_velocity[0] *= -elasticity
    elif ball_pos[0] > (dims[0] - radius) and ball_velocity[0] > 0:
        ball_velocity[0] *= -elasticity


done = False
while not done:
    update_ball()
    screen.fill(black)
    draw()
    pygame.display.flip()
~~~~

------------------------------------------------------------------------------

### Including previously unincluded libraries
In this file the system must be capable of working with newly-imported modules.

It also includes a change in the lines within the main loop. As these lines are
anonymous it will be difficult to change them.

~~~~{.python}
# File: interactive.py
# Allows the user to control the ball
import pygame
import pygame.gfxdraw
from pygame.locals import *


# Some colours
black = (0, 0, 0)
purple = (116, 4, 181)

pygame.init()
dims = (640, 480)
screen = pygame.display.set_mode(dims)
clock = pygame.time.Clock()

ball_pos = (120, 120)
ball_velocity = (800, -400)
gravity = 900
friction = 0.8
elasticity = 0.7
radius = 40
stick = False


def darken((R, G, B)):
    """ Make a colour a little darker """
    return (R / 4, G / 4, B / 4)


def draw():
    pos = [int(round(n)) for n in ball_pos]
    pygame.gfxdraw.filled_circle(screen, pos[0], pos[1], radius, purple)
    pygame.gfxdraw.filled_circle(screen, pos[0], pos[1], radius - 5, darken(purple))


def update_ball():
    global ball_pos, ball_velocity
    dt = clock.tick(60)
    dt_seconds = dt / 1000.0
    ball_pos = [a + (b * dt_seconds) for (a, b) in zip(ball_pos, ball_velocity)]
    ball_velocity = [n * (1 - (friction * dt_seconds)) for n in ball_velocity]
    ball_velocity[1] += gravity * dt_seconds
    left = top = radius
    right = dims[0] - radius
    bottom = dims[1] - radius
    if ball_pos[1] <= top:
        ball_pos[1] = top
        if not stick and ball_velocity[1] < 0:
            ball_velocity[1] *= -elasticity
        elif stick:
            ball_velocity[1] = 0
    elif ball_pos[1] >= bottom:
        ball_pos[1] = bottom
        if not stick and ball_velocity[1] > 0:
            ball_velocity[1] *= -elasticity
        elif stick:
            ball_velocity[1] = 0

    if ball_pos[0] <= left:
        ball_pos[0] = left
        if not stick and ball_velocity[0] < 0:
            ball_velocity[0] *= -elasticity
        elif stick:
            ball_velocity[0] = 0
    elif ball_pos[0] >= right:
        ball_pos[0] = right
        if not stick and ball_velocity[0] > 0:
            ball_velocity[0] *= -elasticity
        elif stick:
            ball_velocity[0] = 0


def handle_events():
    global stick, done
    for event in pygame.event.get():
        if event.type == KEYDOWN:
            keys = pygame.key.get_pressed()
            if keys[K_w]:
                ball_velocity[1] -= gravity
            if keys[K_s]:
                ball_velocity[1] += gravity
            if keys[K_a]:
                ball_velocity[0] -= gravity
            if keys[K_d]:
                ball_velocity[0] += gravity
            if keys[K_SPACE]:
                stick = True
            if keys[K_q]:
                done = True
        elif event.type == KEYUP:
            keys = pygame.key.get_pressed()
            if not keys[K_SPACE]:
                stick = False


done = False
while not done:
    handle_events()
    update_ball()
    screen.fill(black)
    draw()
    pygame.display.flip()
~~~~

------------------------------------------------------------------------------

## Multiple file example
This example includes classes and modules, all of which will need to be tracked
by the system.

### Main script
~~~~{.python}
#!/usr/bin/env python2
# File: main.py
import pygame
import pygame.gfxdraw
import pygame.font
from pygame.locals import *

from pokebrawl import control, frame


def darken((R, G, B)):
    """ Make a colour a little darker """
    return (R / 4, G / 4, B / 4)

# Some colours
white = (255, 255, 255)
black = (0, 0, 0)
purple = (116, 4, 181)

pygame.init()
dims = (1280, 800)
screen = pygame.display.set_mode(dims)
clock = pygame.time.Clock()
frame = frame.Frame(screen)

ubuntu_font_file = pygame.font.match_font('ubuntumono')
ubuntu_font = pygame.font.Font(ubuntu_font_file, 30)

background = pygame.image.load('bg.jpg').convert()
frame.set_background(background)

gravity = 1000
friction = 0.8
elasticity = 0.7
actual_fps = 60.0
stick = False


class Ball(object):
    def __init__(self, (x, y), (vx, vy), radius):
        self.x, self.y = (x, y)
        self.vx, self.vy = (vx, vy)
        self.radius = radius

        r = self.radius
        self._surface = pygame.Surface((r * 2, r * 2))
        self._surface.set_colorkey((0, 0, 0))
        pygame.gfxdraw.filled_circle(self._surface,
                                     r, r,
                                     self.radius,
                                     purple)
        pygame.gfxdraw.filled_circle(self._surface,
                                     r, r,
                                     self.radius - 5,
                                     darken(purple))

    def set_position(self, pos):
        self.x, self.y = pos

    def get_position(self):
        return (self.x, self.y)

    def set_velocity(self, v):
        self.vx, self.vy = v

    def get_velocity(self):
        return (self.vx, self.vy)

    def draw(self):
        return (self._surface, self.x, self.y, 1)


class Enclosure(object):
    def __init__(self, dims):
        self.dims = self.w, self.h = dims
        self._surface = pygame.Surface(self.dims)
        self._surface.set_colorkey((0, 0, 0))
        pygame.draw.rect(self._surface, purple, (0, 0, self.w, self.h), 10)

    def draw(self):
        return (self._surface, 0, 0, 0)


class TextVar(object):
    def __init__(self, initial_text, font, colour, position):
        self.text = initial_text
        font_file = pygame.font.match_font(font)
        self.font = pygame.font.Font(font_file, 30)
        self.colour = colour
        self.x, self.y = position

    def set_text(self, text):
        self.text = text

    def draw(self):
        t = self.font.render(self.text, True, self.colour)
        return (t, self.x + frame.x, self.y + frame.y, 10)


ball = Ball((120, 120), (800, -400), 40)
enclosure = Enclosure((14000, 300))
fps_text = TextVar("60.0 fps", 'ubuntumono', white, (0, 0))
frame.add_drawable(ball)
frame.add_drawable(enclosure)
frame.add_drawable(fps_text)


def update_ball():
    global ball, frame, fps_text
    if abs(ball.vx) < 1:
        ball.vx = 0
    if abs(ball.vy) < 1:
        ball.vy = 0
    dt = clock.tick(60)
    fps_text.set_text("%3.1f fps" % (1000 / float(dt)))
    dt_seconds = dt / 1000.0
    ball.set_position([a + (b * dt_seconds)
                      for (a, b) in zip(ball.get_position(),
                                        ball.get_velocity())])
    ball.set_velocity([n * (1 - (friction * dt_seconds))
                      for n in ball.get_velocity()])
    ball.vy += gravity * dt_seconds
    left = top = 0
    right = enclosure.w - (ball.radius * 2)
    bottom = enclosure.h - (ball.radius * 2)
    if ball.y <= top:
        ball.y = top
        if not stick and ball.vy < 0:
            ball.vy *= -elasticity
        elif stick:
            ball.vy = 0
    elif ball.y >= bottom:
        ball.y = bottom
        if not stick and ball.vy > 0:
            ball.vy *= -elasticity
        elif stick:
            ball.vy = 0

    if ball.x <= left:
        ball.x = left
        if not stick and ball.vx < 0:
            ball.vx *= -elasticity
        elif stick:
            ball.vx = 0
    elif ball.x >= right:
        ball.x = right
        if not stick and ball.vx > 0:
            ball.vx *= -elasticity
        elif stick:
            ball.vx = 0

    frame.x = ball.x - (dims[0] / 2)
    frame.y = ball.y - (dims[1] / 2)


def press(direction):
    power = 1800

    def inner_func():
        if direction == "up":
            ball.vy -= power
        elif direction == "down":
            ball.vy += power
        elif direction == "left":
            ball.vx -= power
        elif direction == "right":
            ball.vx += power
    return inner_func


def stop_ball():
    global ball
    ball.set_velocity([0, 0])


def enable_stick():
    global stick
    stick = True


def disable_stick():
    global stick
    stick = False


def quit():
    global done
    done = True

c = control.Control()
c.register(K_w, press("up"), control.ON_PRESS)
c.register(K_a, press("left"), control.ON_PRESS)
c.register(K_s, press("down"), control.ON_PRESS)
c.register(K_d, press("right"), control.ON_PRESS)
c.register(K_j, stop_ball, control.WHILE_PRESSED)
c.register(K_SPACE, enable_stick, control.ON_PRESS)
c.register(K_SPACE, disable_stick, control.ON_RELEASE)
c.register(K_q, quit, control.ON_PRESS)
done = False
while not done:
    c.handle_events()
    update_ball()

    frame.render()
~~~~

------------------------------------------------------------------------------

### Keyboard control library

~~~~{.python}
# File: pokebrawl/control.py
import pygame
from pygame.locals import *

# Some local constants
ON_PRESS = 1
WHILE_PRESSED = 2
ON_MOUSE_DOWN = 3
WHILE_MOUSE_DOWN = 4
ON_RELEASE = 5


class Control(object):
    def __init__(self):
        self.callbacks = {}

    def register(self, key, callback, method=ON_PRESS):
        try:
            self.callbacks[method][key] = callback
        except KeyError:
            self.callbacks[method] = {key: callback}

    def handle_events(self):
        keys = pygame.key.get_pressed()
        for event in pygame.event.get():
            if ON_PRESS in self.callbacks and event.type == KEYDOWN:
                for key, callback in self.callbacks[ON_PRESS].iteritems():
                    if key == event.key:
                        callback()
            elif ON_RELEASE in self.callbacks and event.type == KEYUP:
                for key, callback in self.callbacks[ON_RELEASE].iteritems():
                    if key == event.key:
                        callback()
        if WHILE_PRESSED in self.callbacks:
            for key, callback in self.callbacks[WHILE_PRESSED].iteritems():
                if keys[key]:
                    callback()
~~~~

------------------------------------------------------------------------------

### Camera and drawing library

~~~~{.python}
# File frame.py
import pygame

bg_scroll = 0.2


class Frame(object):
    def __init__(self, screen):
        self.objects = []
        self.screen = screen
        self.following = None
        self.x, self.y = 0, 0
        self.w, self.h = screen.get_size()
        self.background = None

    def add_drawable(self, drawable):
        self.objects.append(drawable)

    def get_position(self):
        return (self.x, self.y)

    def set_background(self, background):
        self.background = background

    def render(self):
        if self.background is not None:
            self.screen.blit(self.background,
                            ((bg_scroll * -self.x) - 150,
                             (bg_scroll * -self.y) - 100))
        layers = {}
        for obj in self.objects:
            (surface, x, y, z) = obj.draw()
            try:
                layers[z].append((surface, x, y))
            except KeyError:
                layers[z] = [(surface, x, y)]

        for key in sorted(layers.keys()):
            for (surface, x, y) in layers[key]:
                self.screen.blit(surface, (x - self.x, y - self.y))

        pygame.display.flip()
~~~~

------------------------------------------------------------------------------

\newpage

# Bibliography
